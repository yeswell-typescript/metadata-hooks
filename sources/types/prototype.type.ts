import { Constructor } from './constructor.type';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
type Prototype = Record <symbol | number | string, any> & {
	constructor: Constructor;
};

export { Prototype };
