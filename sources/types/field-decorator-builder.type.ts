import { FieldDecorator } from './field-decorator.type';

type FieldDecoratorBuilder <MetadataValue> = (metadataValue: MetadataValue) => FieldDecorator;

export { FieldDecoratorBuilder };
