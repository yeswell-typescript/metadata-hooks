type FieldDecorator = (PropertyDecorator & MethodDecorator);

export { FieldDecorator };
