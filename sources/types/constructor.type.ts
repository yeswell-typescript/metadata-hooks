// eslint-disable-next-line @typescript-eslint/no-unsafe-function-type
type Constructor <C extends Function = Function> = C;

export { Constructor };
