import 'reflect-metadata';

import { FIELD_DECORATOR_BUILDERS, FIELD_KEYS } from '../constants';
import { FieldDecorator, FieldDecoratorBuilder, FieldKey, Prototype } from '../types';

function useClassMetadata <MetadataKey extends (symbol | string), MetadataValue> (
	metadataKey: MetadataKey,
	decoratorBuilder: FieldDecoratorBuilder <MetadataValue>,
): FieldDecorator {
	return (prototype: Prototype, fieldKey: FieldKey) => {
		const constructor = prototype.constructor;

		const fieldKeys: Set <FieldKey> = (Reflect.getMetadata(FIELD_KEYS, constructor) ?? new Set());

		if (!Reflect.hasMetadata(FIELD_KEYS, constructor)) {
			Reflect.defineMetadata(FIELD_KEYS, fieldKeys, constructor);
		}

		fieldKeys.add(fieldKey);

		const decoratorBuilders: Map <MetadataKey, FieldDecoratorBuilder <MetadataValue>> = (
			Reflect.getMetadata(FIELD_DECORATOR_BUILDERS, prototype, fieldKey) ?? new Map()
		);

		if (!Reflect.hasMetadata(FIELD_DECORATOR_BUILDERS, prototype, fieldKey)) {
			Reflect.defineMetadata(FIELD_DECORATOR_BUILDERS, decoratorBuilders, prototype, fieldKey);
		}

		decoratorBuilders.set(metadataKey, decoratorBuilder);
	};
}

export { useClassMetadata };
