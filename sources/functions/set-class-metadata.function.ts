import 'reflect-metadata';

import { FIELD_DECORATOR_BUILDERS, FIELD_KEYS } from '../constants';
import { Constructor, FieldDecoratorBuilder, FieldKey, Prototype } from '../types';

function setClassMetadata <MetadataKey extends (symbol | string), MetadataValue> (
	metadataKey: MetadataKey,
	metadataValue: MetadataValue,
): ClassDecorator {
	return (constructor: Constructor) => {
		Reflect.defineMetadata(metadataKey, metadataValue, constructor);

		const fieldKeys: Set <FieldKey> | null = (Reflect.getMetadata(FIELD_KEYS, constructor) ?? null);

		if (fieldKeys === null) {
			return;
		}

		const prototype: Prototype = constructor.prototype;
		const keysForDeletion: FieldKey[] = [];

		for (const fieldKey of fieldKeys.values()) {
			const decoratorBuilders: Map <MetadataKey, FieldDecoratorBuilder <MetadataValue>> = (
				Reflect.getMetadata(FIELD_DECORATOR_BUILDERS, prototype, fieldKey)
			);

			const decoratorBuilder = (decoratorBuilders.get(metadataKey) ?? null);

			if (decoratorBuilder === null) {
				continue;
			}

			const decorator = decoratorBuilder(metadataValue);
			const descriptor = Object.getOwnPropertyDescriptor(prototype, fieldKey);

			const updatedDescriptor = Reflect.decorate([decorator], prototype, fieldKey, descriptor);

			if (updatedDescriptor) {
				Object.defineProperty(prototype, fieldKey, updatedDescriptor);
			}

			decoratorBuilders.delete(metadataKey);

			if (decoratorBuilders.size === 0) {
				Reflect.deleteMetadata(FIELD_DECORATOR_BUILDERS, prototype, fieldKey);

				keysForDeletion.push(fieldKey);
			}
		}

		keysForDeletion.forEach((key) => fieldKeys.delete(key));

		if (fieldKeys.size === 0) {
			Reflect.deleteMetadata(FIELD_KEYS, constructor);
		}
	};
}

export { setClassMetadata };
